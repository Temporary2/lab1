groupmates = [
    {
        'name': 'Александр',
        'surname': 'Иванов',
        'exams': ['Информатика', 'ЭЭиС', 'Web'],
        'marks': [4, 3, 5]
    },
    {
        'name': 'Иван',
        'surname': 'Петров',
        'exams': ['История', 'АиГ', 'КТП'],
        'marks': [4, 4, 4]
    },
    {
        'name': 'Кирилл',
        'surname': 'Смирнов',
        'exams': ['Философия', 'ИС', 'КТП'],
        'marks': [5, 5, 5]
    },
    {
        'name': 'Игорь',
        'surname': 'Алексеев',
        'exams': ['Вычмат', 'Анализ', 'ТФКП'],
        'marks': [3, 5, 4]
    },
    {
        'name': 'Владимир',
        'surname': 'Мономах',
        'exams': ['История', 'Право', 'Лингвистика'],
        'marks': [4,3,4]
    }
]


def print_students(students, av_mark):
    print('Имя'.ljust(15), 
        'Фамилия'.ljust(10), 
        'Экзамены'.ljust(30), 
        'Оценки'.ljust(20))
    for student in students:
        if sum(student['marks'])/len(student['marks']) <= av_mark: continue
        print(student['name'].ljust(15), 
            student['surname'].ljust(10), 
            ' '.join(map(str,student['exams'])).ljust(30), 
            ' '.join(map(str,student['marks'])).ljust(20))
av_mark = float(input('Enter the average mark to filter:\n'))
print_students(groupmates, av_mark)